package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("SELECT DISTINCT new com.bsa.springdata.office.OfficeDto(o.id, o.city, o.address) FROM Office o " +
            "INNER JOIN User u ON u.office.id = o.id " +
            "INNER JOIN Team t ON t.id = u.team.id " +
            "INNER JOIN Technology tec ON tec.id = t.technology.id " +
            "WHERE tec.name = :technology")
    List<OfficeDto> getByTechnology(@Param("technology") String technology);

    @Transactional
    @Modifying
    @Query(value = "UPDATE offices o SET address = :newAddress " +
            "WHERE (SELECT COUNT(*) FROM users u WHERE (u.team_id is not NULL AND u.office_id = o.id))>0 " +
            "AND o.address = :oldAddress",
            nativeQuery = true)
    void updateAddress(@Param("oldAddress")String oldAddress, @Param("newAddress")String newAddress);


    Optional<OfficeDto> getOfficeByAddress(String addres);

}
