package com.bsa.springdata.office;

import com.bsa.springdata.db.BaseEntity;
import com.bsa.springdata.user.User;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

// TODO: Map table offices to this entity
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper=false)
@Table(name = "offices")
public class Office extends BaseEntity {
    @Column(name = "city", columnDefinition="TEXT")
    private String city;
    @Column(name = "address", columnDefinition="TEXT")
    private String address;
    @OneToMany(mappedBy = "office", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<User> users;
}
