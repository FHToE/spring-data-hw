package com.bsa.springdata.role;

import com.bsa.springdata.db.BaseEntity;
import com.bsa.springdata.user.User;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

// TODO: Map table roles to this entity
@Entity
@Data
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@Table(name = "roles")
public class Role extends BaseEntity {
    @Column(name = "name", columnDefinition="TEXT")
    private String name;
    @Column(name = "code", columnDefinition="TEXT")
    private String code;
    @ManyToMany(mappedBy = "roles")
    private Set<User> users = new HashSet<>();
}
