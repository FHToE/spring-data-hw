package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    //void deleteRolesByCodeAndUsersIsNull(String code);
    @Transactional
    void deleteRolesByCodeEqualsAndUsersIsNull(String code);
}
