package com.bsa.springdata.user;

import com.bsa.springdata.user.dto.UserDto;
import com.bsa.springdata.user.dto.UserShortDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findUsersByLastNameIsStartingWith (String lastName, Pageable pageable);

    @Query("SELECT new com.bsa.springdata.user.dto.UserShortDto(u.id, " +
            "u.firstName, u.lastName, u.experience) from User u " +
            "INNER JOIN Office o ON o.id=u.office.id " +
            "WHERE o.city = :city ORDER BY u.lastName")
    List<UserShortDto> findByCity(@Param("city") String city);

    List<User> findUsersByExperienceIsGreaterThanEqual (int experience, Sort sort);

    @Query("SELECT new com.bsa.springdata.user.dto.UserShortDto(u.id, " +
            "u.firstName, u.lastName, u.experience) from User u " +
            "INNER JOIN Office o ON o.id=u.office.id " +
            "INNER JOIN Team t ON t.id=u.team.id " +
            "WHERE o.city = :city AND t.room = :room " +
            "ORDER BY u.lastName")
    List<UserShortDto> findByRoomAndCity(@Param("city")String city, @Param("room")String room);

    int deleteUsersByExperienceIsLessThan(int experience);

}
