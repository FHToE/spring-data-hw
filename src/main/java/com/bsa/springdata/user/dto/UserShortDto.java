package com.bsa.springdata.user.dto;

import com.bsa.springdata.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
@Builder
public class UserShortDto {
    private final UUID id;
    private final String firstName;
    private final String lastName;
    private final int experience;

    public static UserShortDto fromEntity(User user) {
        return UserShortDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .experience(user.getExperience()).build();
    }
}
