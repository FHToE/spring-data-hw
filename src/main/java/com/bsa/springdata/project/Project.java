package com.bsa.springdata.project;

import com.bsa.springdata.db.BaseEntity;
import com.bsa.springdata.team.Team;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

// TODO: Map table projects to this entity
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Builder
@Table(name = "projects")
public class Project extends BaseEntity {
    @Column(name = "name", columnDefinition="TEXT")
    private String name;
    @Column(name = "description", columnDefinition="TEXT")
    private String description;
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Team> teams;
}
