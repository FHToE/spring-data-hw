package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import com.bsa.springdata.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.*;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        Pageable top5 = PageRequest.of(0,5);
        return projectRepository.findTop5ByTechnology(technology, top5);
        // TODO: Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
    }

    public Optional<ProjectDto> findTheBiggest() {
        Pageable top1 = PageRequest.of(0,1);
        return Optional.of(projectRepository.findTheBiggest(top1).get(0)); //test not passed
        // TODO: Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
    }

    public List<ProjectSummaryDto> getSummary() {
        //var result = projectRepository.getSummary();
        //System.out.println("____________");
        //result.forEach(x-> System.out.println(x.getName()+ " " + x.getTeamsNumber() + " " + x.getDevelopersNumber() + " " + x.getTechnologies()));
        return projectRepository.getSummary();
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
    }

    public int getCountWithRole(String role) {
        return projectRepository.getCountWithRole(role);
        // TODO: Use a single query
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        Technology tech = Technology.builder().description(createProjectRequest.getTechDescription())
                .link(createProjectRequest.getTechLink()).name(createProjectRequest.getTech()).build();
        technologyRepository.save(tech);
        Project project = Project.builder().description(createProjectRequest.getProjectDescription())
                .name(createProjectRequest.getProjectName()).teams(new ArrayList<Team>()).build();
        projectRepository.save(project);
        Team team = Team.builder().area(createProjectRequest.getTeamArea()).name(createProjectRequest.getTeamName())
                .room(createProjectRequest.getTeamRoom()).project(project)
                .technology(tech).users(new ArrayList<User>()).build();
        teamRepository.save(team);
        System.out.println("saved");
        return project.getId();
        // TODO: Use common JPARepository methods. Build entities in memory and then persist them
    }
}
