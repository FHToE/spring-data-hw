package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {


    @Query("SELECT new com.bsa.springdata.project.dto.ProjectDto(p.id, p.name, p.description) FROM Project p " +
            "INNER JOIN Team t ON t.project.id = p.id " +
            "INNER JOIN Technology tec ON  tec.id = t.technology.id " +
            "WHERE tec.name = :technology ORDER BY t.users.size")
    List<ProjectDto> findTop5ByTechnology(@Param("technology") String technology, Pageable pageable);

    @Query("SELECT new com.bsa.springdata.project.dto.ProjectDto(p.id, p.name, p.description) FROM Project p " +
            "INNER JOIN Team t ON t.project.id = p.id " +
            "GROUP BY p.id " +
            "ORDER BY p.teams.size DESC, SUM(t.users.size) DESC, p.name DESC")
    List<ProjectDto> findTheBiggest(Pageable pageable);



    @Query(value = "SELECT COUNT(DISTINCT p.id) FROM projects p " +
            "INNER JOIN teams t ON t.project_id = p.id " +
            "INNER JOIN users u ON u.team_id = t.id " +
            "INNER JOIN User2Role u2r ON u2r.user_id = u.id " +
            "INNER JOIN roles r ON r.id = u2r.role_id " +
            "WHERE r.name = :role",
            nativeQuery = true)
    int getCountWithRole(@Param("role")String role);

    @Query(value = "SELECT p.name AS name , COUNT(DISTINCT t.*) " +
            "AS teamsNumber, COUNT(u.*) AS developersNumber, STRING_AGG(DISTINCT CAST(tec.name AS VARCHAR),',') AS technologies " +
            "FROM projects p " +
            "INNER JOIN teams t ON p.id = t.project_id " +
            "INNER JOIN users u ON t.id = u.team_id " +
            "INNER JOIN technologies tec ON tec.id = t.technology_id " +
            "GROUP BY p.name " +
            "ORDER BY p.name",
            nativeQuery = true)
    List<ProjectSummaryDto> getSummary();
}