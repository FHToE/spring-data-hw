package com.bsa.springdata.team;

import com.bsa.springdata.db.BaseEntity;
import com.bsa.springdata.project.Project;
import com.bsa.springdata.user.User;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

// TODO: Map table teams to this entity
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Builder
@AllArgsConstructor
@Table(name = "teams")
public class Team extends BaseEntity {
    @Column(name = "name", columnDefinition="TEXT")
    private String name;
    @Column(name = "room", columnDefinition="TEXT")
    private String room;
    @Column(name = "area", columnDefinition="TEXT")
    private String area;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "project_id")
    private Project project;
    @OneToMany(mappedBy = "team", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<User> users;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "technology_id")
    private Technology technology;

}
