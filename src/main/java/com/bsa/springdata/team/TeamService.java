package com.bsa.springdata.team;

import com.bsa.springdata.team.dto.TechnologyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        Optional<TechnologyDto> technologyDto = technologyRepository.findByName(newTechnologyName);
        Optional<TechnologyDto> oldtechnologyDto = technologyRepository.findByName(oldTechnologyName);
        if (technologyDto.isEmpty() || oldtechnologyDto.isEmpty()) return;
        teamRepository.update(Technology.fromDto(technologyDto.get()), devsNumber, oldtechnologyDto.get().getId());
        // TODO: You can use several queries here. Try to keep it as simple as possible
    }

    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
        // TODO: Use a single query. You need to create a native query
    }
}
