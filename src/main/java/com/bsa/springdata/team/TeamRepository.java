package com.bsa.springdata.team;

import com.bsa.springdata.team.dto.TeamDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    int countByTechnologyName(String technology);
    Optional<TeamDto> findByName(String name);

    @Transactional
    @Modifying
    @Query("UPDATE Team t SET t.technology= :tec WHERE (t.users.size < :devs  AND t.technology.id = :old)")
    void update(@Param("tec")Technology tec, @Param("devs")int devs, @Param("old")UUID old);

    @Transactional
    @Modifying
    @Query(value = "UPDATE teams SET name = concat(t.name, '_', pr.name, '_', tec.name) " +
            "FROM teams as t " +
            "INNER JOIN projects pr ON t.project_id = pr.id " +
            "INNER JOIN technologies tec ON t.technology_id = tec.id " +
            "WHERE teams.name = :teamname",
            nativeQuery = true)
    public void normalizeName(@Param("teamname")String teamname); //TODO
}
