package com.bsa.springdata.team;

import com.bsa.springdata.team.dto.TechnologyDto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TechnologyRepository extends JpaRepository<Technology, UUID> {
    Optional<TechnologyDto> findByName(String name);
}
