package com.bsa.springdata.team;

import com.bsa.springdata.db.BaseEntity;
import com.bsa.springdata.team.dto.TechnologyDto;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

// TODO: Map table technologies to this entity
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@Builder
@AllArgsConstructor
@Table(name = "technologies")
public class Technology extends BaseEntity {
    @Column(name = "name", columnDefinition="TEXT")
    private String name;
    @Column(name = "description", columnDefinition="TEXT")
    private String description;
    @Column(name = "link", columnDefinition="TEXT")
    private String link;

    public static Technology fromDto(TechnologyDto dto) {
        Technology tec = Technology.builder()
                .description(dto.getDescription())
                .link(dto.getLink())
                .name(dto.getName())
                .build();
        tec.setId(dto.getId());
        return tec;
    }
}