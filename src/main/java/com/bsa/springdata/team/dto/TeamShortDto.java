package com.bsa.springdata.team.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
@Builder
public class TeamShortDto {
    private String name;
    private String room;
    private String area;
    private UUID projectId;
    private UUID technologyId;
}
