package com.bsa.springdata.db;

import com.bsa.springdata.office.Office;
import com.bsa.springdata.office.OfficeRepository;
import com.bsa.springdata.project.Project;
import com.bsa.springdata.project.ProjectRepository;
import com.bsa.springdata.role.Role;
import com.bsa.springdata.role.RoleRepository;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import com.bsa.springdata.user.User;
import com.bsa.springdata.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class DbSeeder {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private OfficeRepository officeRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private UserRepository userRepository;

    List<Office> offices = new ArrayList<>();
    List<Project> projects = new ArrayList<>();
    List<Technology> technologies = new ArrayList<>();
    List<Role> roles = new ArrayList<>();
    List<Team> teams = new ArrayList<>();
    List<User> users = new ArrayList<>();


    @EventListener
    public void seed (ContextRefreshedEvent event) {
        List<User> u = jdbcTemplate.query("SELECT * FROM users", (resultSet, rowNum) -> null);
        if(u == null || u.size() <= 0) {
            seedTechnologiesTable();
            seedRolesTable();
            seedProjectsTable();
            seedOfficesTable();
            seedTeamsTable();
            seedUsersTable();
        }
    }

    private void seedTechnologiesTable() {
        technologyRepository.saveAll(getTechnologies());
    }

    private void seedRolesTable() {
        roleRepository.saveAll(getRoles());
    }

    private void seedOfficesTable() {
        officeRepository.saveAll(getOffices());
    }

    private void seedTeamsTable() {
        teamRepository.saveAll(getTeams());
    }

    private void seedProjectsTable() {
        projectRepository.saveAll(getProjects());
    }

    private void seedUsersTable() {
        userRepository.saveAll(getUsers());
    }

    private List<User> getUsers() {
        User user1 = User.builder().firstName("John").lastName("Ivanov")
                .experience(99).office(offices.get(rand())).team(teams.get(rand())).roles(getRandomRoles()).build();
        User user2 = User.builder().firstName("Patrick").lastName("Petrov")
                .experience(15).office(offices.get(rand())).team(teams.get(rand())).roles(getRandomRoles()).build();
        User user3 = User.builder().firstName("Den").lastName("Lenin")
                .experience(51).office(offices.get(rand())).team(teams.get(rand())).roles(getRandomRoles()).build();
        User user4 = User.builder().firstName("Peter").lastName("Parker")
                .experience(19).office(offices.get(rand())).team(teams.get(rand())).roles(getRandomRoles()).build();
        User user5 = User.builder().firstName("Anna").lastName("Ivanova")
                .experience(91).office(offices.get(rand())).team(teams.get(rand())).roles(getRandomRoles()).build();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        return users;
    }

    private Set<Role> getRandomRoles() {
        Set<Role> randroles = new HashSet<>(roles);
        for (int i = 0; i<4; i++) randroles.remove(roles.get((int) (Math.random()*5)));
        return randroles;
    }

    private List<Office> getOffices() {
        Office kyiv = new Office("Kiev", "Kreshatyk 999", new ArrayList<User>());
        Office lviv = new Office("Lvov", "Lesi Ukrainki 550", new ArrayList<User>());
        Office kharkiv = new Office("Kharkov", "Shevchenka 153", new ArrayList<User>());
        Office vinnitsa = new Office("Vinnitsa", "Franka 120", new ArrayList<User>());
        Office zhytomir = new Office("Zhytomir", "Mazepu 78", new ArrayList<User>());
        offices.add(kyiv);
        offices.add(lviv);
        offices.add(kharkiv);
        offices.add(vinnitsa);
        offices.add(zhytomir);
        return offices;
    }

    private List<Team> getTeams() { // mb need add info to project and technology?
        Team team1 = new Team("team1", "55", "dunno what is it, some area", projects.get(rand()),
                new ArrayList<User>(), technologies.get(rand()));
        Team team2 = new Team("team2", "234", "dunno what is it, some area", projects.get(rand()),
                new ArrayList<User>(), technologies.get(rand()));
        Team team3 = new Team("team3", "231", "dunno what is it, some area", projects.get(rand()),
                new ArrayList<User>(), technologies.get(rand()));
        Team team4 = new Team("team4", "234", "dunno what is it, some area", projects.get(rand()),
                new ArrayList<User>(), technologies.get(rand()));
        Team team5 = new Team("team5", "85", "dunno what is it, some area", projects.get(rand()),
                new ArrayList<User>(), technologies.get(rand()));
        teams.add(team1);
        teams.add(team2);
        teams.add(team3);
        teams.add(team4);
        teams.add(team5);
        return teams;
    }

    private int rand() {
        return (int) (Math.random()*5);
    }


    private List<Project> getProjects() {
        Project vk = new Project("VK", "social network", new ArrayList<Team>());
        Project fb = new Project("facebook", "social network", new ArrayList<Team>());
        Project ig = new Project("instagram", "photo social network", new ArrayList<Team>());
        Project ba = new Project("binary academy", "my stydying", new ArrayList<Team>());
        Project ba2 = new Project("binary academy", "my studying, projectlevel", new ArrayList<Team>());
        projects.add(vk);
        projects.add(fb);
        projects.add(ig);
        projects.add(ba);
        projects.add(ba2);
        return projects;

    }

    private List<Technology> getTechnologies() {
        Technology java = new Technology("java", "Java is a general-purpose programming language " +
                "that is class-based, object-oriented, and designed to have as few implementation dependencies as " +
                "possible.", "https://en.wikipedia.org/wiki/Java_(programming_language)");
        Technology php = new Technology("php", "PHP is a popular general-purpose scripting language that " +
                "is especially suited to web development.", "https://en.wikipedia.org/wiki/PHP");
        Technology js = new Technology("JavaScript ", "JavaScript (/ˈdʒɑːvəˌskrɪpt/),[6] often " +
                "abbreviated as JS, is a programming language that conforms to the ECMAScript specification.",
                "https://en.wikipedia.org/wiki/JavaScript");
        Technology dotnet = new Technology(".NET", ".NET Framework (pronounced as \"dot net\") is " +
                "a software framework developed by Microsoft that runs primarily on Microsoft Windows.",
                "https://en.wikipedia.org/wiki/.NET_Framework");
        Technology cplus = new Technology("C++", "C++ (/ˌsiːˌplʌsˈplʌs/) is a general-purpose " +
                "programming language created by Bjarne Stroustrup as an extension of the C programming language, " +
                "or \"C with Classes\".", "https://en.wikipedia.org/wiki/C%2B%2B");
        technologies.add(java);
        technologies.add(php);
        technologies.add(js);
        technologies.add(dotnet);
        technologies.add(cplus);
        return technologies;
    }

    private List<Role> getRoles() {
        Role PrM = Role.builder().name("Project Manager").code("PrM").users(new HashSet<User>()).build();
        Role Architect = Role.builder().name("Architect").code("Arch").users(new HashSet<User>()).build();
        Role BusinessAnalyst = Role.builder().name("Business Analyst").code("BA").users(new HashSet<User>()).build();
        Role AdvancedDeveloper = Role.builder().name("Developer").code("Dev").users(new HashSet<User>()).build();
        Role jun = Role.builder().name("junior").code("jun").users(new HashSet<User>()).build();
        roles.add(PrM);
        roles.add(Architect);
        roles.add(BusinessAnalyst);
        roles.add(AdvancedDeveloper);
        roles.add(jun);
        return roles;
    }


}
